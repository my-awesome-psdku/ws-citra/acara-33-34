function [BW, Img] = remove_bg(input_image)
    % Konversi gambar input menjadi gambar dalam ruang warna Lab
    lab_image = rgb2lab(input_image);
    
    % Ambil saluran L* (luminance) dari gambar Lab
    L = lab_image(:, :, 1);
    
    % Ambil saluran a* dan b* (komponen warna) dari gambar Lab
    a = lab_image(:, :, 2);
    b = lab_image(:, :, 3);
    
    % Tentukan nilai ambang untuk segmentasi warna (sesuaikan sesuai kebutuhan)
    threshold_value = 40;  % Nilai ambang yang sesuai dapat berbeda-beda
    
    % Segmentasi warna dengan ambang yang telah ditentukan
    binary_image = (a < threshold_value) & (b < threshold_value);
    
    % Bersihkan gambar biner
    se = strel('disk', 10);  % Struktur elemen untuk operasi erosi
    binary_image = imerode(binary_image, se);
    binary_image = imdilate(binary_image, se);
    
    % Terapkan gambar biner sebagai masker ke gambar asli
    R = input_image(:, :, 1);
    G = input_image(:, :, 2);
    B = input_image(:, :, 3);
    
    R(~binary_image) = 255;  % Ubah latar belakang yang tidak dipilih menjadi putih
    G(~binary_image) = 255;
    B(~binary_image) = 255;
    
    % Gabungkan saluran R, G, B menjadi gambar hasil tanpa latar belakang
    Img = cat(3, R, G, B);
    
    % Hasilkan gambar biner yang digunakan sebagai masker
    BW = binary_image;
end
