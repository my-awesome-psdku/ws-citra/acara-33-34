for g = 1:10
    gambarl = imread(['bawang', num2str(g), '.JPG']);
    
    gambar = imresize(gambarl, [400, 400]);
    [BW, Img] = remove_bg(gambar);
    redChannel = Img(:, :, 1);
    greenChannel = Img(:, :, 2);
    blueChannel = Img(:, :, 3);
    gray = rgb2gray(Img);
    [M, N] = size(blueChannel);
    IFeature = zeros(M, N);
    
    red_mean = mean(mean(redChannel));
    green_mean = mean(mean(greenChannel));
    blue_mean = mean(mean(blueChannel));

    I = Img;

    [tinggi, lebar] = size(I);
    
    % Inisialisasi matriks GLCM dan variabel total_piksel
    GLCM0 = zeros(256, 256);
    total_piksel0 = 0;

    GLCM45 = zeros(256, 256);
    total_piksel45 = 0;

    GLCM90 = zeros(256, 256);
    total_piksel90 = 0;

    GLCM135 = zeros(256, 256);
    total_piksel135 = 0;
    
    for y = 2:tinggi-1
       for x = 2:lebar-1
            % Sudut 0
            a = I(y, x);
            b = I(y, x + 1);
            GLCM0(a + 1, b + 1) = GLCM0(a + 1, b + 1) + 1;
            total_piksel0 = total_piksel0 + 1;

            % Sudut 45
            a = I(y, x);
            b = I(y - 1, x + 1);
            GLCM45(a + 1, b + 1) = GLCM45(a + 1, b + 1) + 1;
            total_piksel45 = total_piksel45 + 1;

            % Sudut 90
            a = I(y, x);
            b = I(y - 1, x);
            GLCM90(a + 1, b + 1) = GLCM90(a + 1, b + 1) + 1;
            total_piksel90 = total_piksel90 + 1;

            % Sudut 135
            a = I(y, x);
            b = I(y - 1, x - 1);
            GLCM135(a + 1, b + 1) = GLCM135(a + 1, b + 1) + 1;
            total_piksel135 = total_piksel135 + 1;
       end
    end
    
    % Normalisasi matriks GLCM
    GLCM0 = GLCM0 / total_piksel0;
    GLCM45 = GLCM45 / total_piksel45;
    GLCM90 = GLCM90 / total_piksel90;
    GLCM135 = GLCM135 / total_piksel135;
    
    % Perhitungan ASM
    asm0 = 0.0;
    asm45 = 0.0;
    asm90 = 0.0;
    asm135 = 0.0;

    for a = 0:255
        for b = 0:255
            asm0 = asm0 + (GLCM0(a + 1, b + 1) * GLCM0(a + 1, b + 1));
            asm45 = asm45 + (GLCM45(a + 1, b + 1) * GLCM45(a + 1, b + 1));
            asm90 = asm90 + (GLCM90(a + 1, b + 1) * GLCM90(a + 1, b + 1));
            asm135 = asm135 + (GLCM135(a + 1, b + 1) * GLCM135(a + 1, b + 1));
        end
    end
    
    % Perhitungan IDM
    idm0 = 0.0;
    idm45 = 0.0;
    idm90 = 0.0;
    idm135 = 0.0;

    for a = 0:255
        for b = 0:255
            idm0 = idm0 + (GLCM0(a + 1, b + 1) / (1 + (a - b) * (a - b)));
            idm45 = idm45 + (GLCM45(a + 1, b + 1) / (1 + (a - b) * (a - b)));
            idm90 = idm90 + (GLCM90(a + 1, b + 1) / (1 + (a - b) * (a - b)));
            idm135 = idm135 + (GLCM135(a + 1, b + 1) / (1 + (a - b) * (a - b)));
        end
    end
    
    % Perhitungan Kontras
    kontras0 = 0.0;
    kontras45 = 0.0;
    kontras90 = 0.0;
    kontras135 = 0.0;

    for a = 0:255
        for b = 0:255
            kontras0 = kontras0 + (a - b) * (a - b) * GLCM0(a + 1, b + 1);
            kontras45 = kontras45 + (a - b) * (a - b) * GLCM45(a + 1, b + 1);
            kontras90 = kontras90 + (a - b) * (a - b) * GLCM90(a + 1, b + 1);
            kontras135 = kontras135 + (a - b) * (a - b) * GLCM135(a + 1, b + 1);
        end
    end
    
    %Perhitungan Konvarsi
    %Perhiitungan px[] dan py[]
    korelasi0 = 0.0;
    px0 = 0;
    py0 = 0;
    reratax0 = 0.0;
    reratay0 = 0.0;
    stdevx0 = 0.0;
    stdevy0 = 0.0;

    korelasi45 = 0.0;
    px45 = 0;
    py45 = 0;
    reratax45 = 0.0;
    reratay45 = 0.0;
    stdevx45 = 0.0;
    stdevy45 = 0.0;

    korelasi90 = 0.0;
    px90 = 0;
    py90 = 0;
    reratax90 = 0.0;
    reratay90 = 0.0;
    stdevx90 = 0.0;
    stdevy90 = 0.0;

    korelasi135 = 0.0;
    px135 = 0;
    py135 = 0;
    reratax135 = 0.0;
    reratay135 = 0.0;
    stdevx135 = 0.0;
    stdevy135 = 0.0;

    for a = 0:255
        for b = 0:255
            px0 = px0 + a * GLCM0(a + 1, b + 1);
            py0 = py0 + b * GLCM0(a + 1, b + 1);

            px45 = px45 + a * GLCM45(a + 1, b + 1);
            py45 = py45 + b * GLCM45(a + 1, b + 1);

            px90 = px90 + a * GLCM90(a + 1, b + 1);
            py90 = py90 + b * GLCM90(a + 1, b + 1);

            px135 = px135 + a * GLCM135(a + 1, b + 1);
            py135 = py135 + b * GLCM135(a + 1, b + 1);
        end
    end
    
    %Perhitungan Devisi Standart
    for a = 0:255
        for b = 0:255
        stdevx0 = stdevx0 + (a - px0) * (a - px0) * GLCM0(a + 1, b + 1);
        stdevy0 = stdevy0 + (b - py0) * (b - py0) * GLCM0(a + 1, b + 1);
        
        stdevx45 = stdevx45 + (a - px45) * (a - px45) * GLCM45(a + 1, b + 1);
        stdevy45 = stdevy45 + (b - py45) * (b - py45) * GLCM45(a + 1, b + 1);
        
        stdevx90 = stdevx90 + (a - px90) * (a - px90) * GLCM90(a + 1, b + 1);
        stdevy90 = stdevy90 + (b - py90) * (b - py90) * GLCM90(a + 1, b + 1);
        
        stdevx135 = stdevx135 + (a - px135) * (a - px135) * GLCM135(a + 1, b + 1);
        stdevy135 = stdevy135 + (b - py135) * (b - py135) * GLCM135(a + 1, b + 1);
        end
    end
    
    %Perhitungan Korelasi
    korelasi0 = 0.0;
    korelasi45 = 0.0;
    korelasi90 = 0.0;
    korelasi135 = 0.0;

    for a = 0:255
        for b = 0:255
            korelasi0 = korelasi0 + ((a - px0) * (b - py0) * GLCM0(a + 1, b + 1) / (stdevx0 * stdevy0));
            korelasi45 = korelasi45 + ((a - px45) * (b - py45) * GLCM45(a + 1, b + 1) / (stdevx45 * stdevy45));
            korelasi90 = korelasi90 + ((a - px90) * (b - py90) * GLCM90(a + 1, b + 1) / (stdevx90 * stdevy90));
            korelasi135 = korelasi135 + ((a - px135) * (b - py135) * GLCM135(a + 1, b + 1) / (stdevx135 * stdevy135));
        end
    end
    
    % Hasil Fitur
    Warna_red = red_mean;
    Warna_green = green_mean;
    Warna_blue = blue_mean;
    G0_asm = asm0;
    G0_idm = idm0;
    G0_kontras = kontras0;
    G0_korelasi = korelasi0;
    G45_asm = asm45;
    G45_idm = idm45;
    G45_kontras = kontras45;
    G45_korelasi = korelasi45;
    G90_asm = asm90;
    G90_idm = idm90;
    G90_kontras = kontras90;
    G90_korelasi = korelasi90;
    G135_asm = asm135;
    G135_idm = idm135;
    G135_kontras = kontras135;
    G135_korelasi = korelasi135;

    % Simpan fitur ke dalam matriks fitur_post
    fitur_post(g, :) = [Warna_red; Warna_green; Warna_blue; G0_asm; G0_idm; G0_kontras; G0_korelasi;
                       G45_asm; G45_idm; G45_kontras; G45_korelasi; G90_asm; G90_idm; G90_kontras; G90_korelasi;
                       G135_asm; G135_idm; G135_kontras; G135_korelasi];
    
end

% Nama file Excel
filename = 'ekstraksi.xlsx';

% Menulis matriks fitur_post ke file Excel
xlswrite(filename, fitur_post);
